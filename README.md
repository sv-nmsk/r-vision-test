### Install
```
npm i https://bitbucket.org/sv-nmsk/r-vision-test
```
### How to use
You can use next bash-script to create a file:
```bash
touch example.txt
echo "text" >> example.txt
for i in {1..20};
do cat example.txt example.txt > tmp && mv tmp example.txt;
done
```

#### Sender example:

```js
const {Sender} = require('r-vision-test');
const sender = new Sender();

(async() => {
    try {
        await sender.init();
        console.log('Sender initialized');  
        const info = await sender.send('example.txt');
        console.log('Has been sent and saved as:', info.filename + info.extname);
        sender.close();
    } catch(e) {
        console.log('Sender error.', e.message);
        sender.close();
    }
})();
```

#### Receiver example
Must be initialized first
```js
const {Receiver, states} = require('r-vision-test');
const receiver = new Receiver({
    dirname: 'incoming'
});

(async() => {
    try {
        await receiver.init();
        console.log('Receiver initialized');        
        receiver.on(states.RECEIVED, (info) => { 
            console.log('Received:', info.filename + info.extname); 
        });
    } catch(e) {
        console.log('Receiver error.', e.message);
    }
})();
```