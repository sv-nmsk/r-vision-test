const fs = require('fs');
const path = require('path');
const {EventEmitter} = require('events');
const {promisify} = require('util');

const randomstring = require("randomstring");
const NATS = require('nats');
const md5File = require('md5-file/promise');

const states = require('./enum/states')
const REQUEST_TIMEOUT = 1000;
const CONFIG = { json: true, };

const newFilename = () => randomstring.generate({length: 15, capitalization: 'lowercase', });
const toJson = (str) => {
    try {
        return JSON.parse(str);
    } catch (e) {
        return null;
    }
}

class Sender {
    constructor() {}
    
    init() {
        return new Promise((resolve, reject) => {
            this.nats = NATS.connect(CONFIG);
            this.nats.on('error', reject);
            this.nats.on('connect', resolve);
        });
    }

    close() { if (this.nats) this.nats.close(); }

    _isTimeOut(res) {
        return res instanceof NATS.NatsError && res.code === NATS.REQ_TIMEOUT;
    }

    _send(file, info) {
        return new Promise((resolve, reject) => {
            this.nats.requestOne('init', info, {}, REQUEST_TIMEOUT, (res) => {                
                if (this._isTimeOut(res)) { return reject(new Error('Have no receivers')); }                
                info = res;

                const input = fs.createReadStream(file, { encoding: null });
                input.on('error', (e) => { this.emit('error', e); });
                input.on('readable', () => {
                    const data = input.read();
                    this.nats.publish(`transfer.${info.filename}`, { info, data });
                });
                input.on('end', () => {
                    this.nats.requestOne('close', info, {}, REQUEST_TIMEOUT, (response) => {
                        if (this._isTimeOut(response)) { 
                            return reject(new Error('Transfer close timeout'))
                        }
                        md5File(response.filePath)
                            .then((hash) => {
                                return hash === info.hash 
                                    ? resolve(info)
                                    : reject(new Error('Ivalid file hash'));
                            }).catch((e) => {
                                reject(new Error('Can`t get hash', e));
                            })
                        ;
                    });
                });
            });
        })
    }

    async send(file) {
        const exist = await promisify(fs.exists)(file)
        if(!exist) throw new Error(`No such file "${file}"`);

        const hash = await md5File(file);
        const extname = path.extname(file);
        let info = {filename: null, hash, extname};

        return await this._send(file, info);
    }
}

class Receiver extends EventEmitter {
    constructor(opt = {}) {
        super();
        this.dirname = opt.dirname || 'files';
        this.streams = {};
        if (!fs.existsSync(this.dirname)) { fs.mkdirSync(this.dirname); }
    }

    init() {
        return new Promise((resolve, reject) => {
            this.nats = NATS.connect(CONFIG);
            this.nats.on('error', reject);
            this.nats.on('connect', () => {
                this._initAllSubscribers();
                resolve();
            });
        })
    }

    close() { if (this.nats) this.nats.close(); }

    _subInit() {
        this.nats.subscribe('init', (info, reply, subject) => {
            info.filename = newFilename();
            this.nats.publish(reply, info);
        });
    }

    _subTransfer() {
        this.nats.subscribe('transfer.*', (msg, reply, subject) => {
            const id = subject.replace('transfer\.', '');
            
            // create write stream if not exists
            if (!this.streams[id]) {
                this.streams[msg.info.filename] = fs.createWriteStream(
                    `${this.dirname}/${msg.info.filename}${msg.info.extname}`,
                    { encoding: null }
                );
            }
            // write chunk if not null
            if (!msg.data) {
                this.streams[id].close();
                delete this.streams[id];
            } else {
                this.streams[id].write(new Buffer(msg.data));
            }
        });
    }
    
    _subClose() {
        this.nats.subscribe('close', (info, reply) => {
            const filePath = path.format({dir: path.join(__dirname, this.dirname), name: info.filename, ext: info.extname});
            this.emit('received', info);
            this.nats.publish(reply, {filePath});
        });
    }

    _initAllSubscribers() {
        this._subInit();
        this._subTransfer();
        this._subClose();
    }
}

module.exports = {Receiver, Sender, states};